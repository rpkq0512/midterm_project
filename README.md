# Software Studio 2019 Spring Midterm Project

## Topic
* Project Name : [ChatRoom]
* Key functions (add/delete)
    1. [ChatRoom]
    
* Other functions (add/delete)
    1. [xxxx]
    2. [xxxx]
    3. [xxxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：[https://midterm-project-491bc.firebaseapp.com/choose-room]

# Components Description : 
1. [MembershipMechanism] : 從 login page 可連結到 sign up page，在這兩個頁面可以 custom 和 google 的登錄和註冊。google註冊要求輸入 username 。
2. [FirebasePage] : 有 npm run build 之後 deploy
3. [Database] : 在 choose room page 和 chat page 皆有使用到 database read/write。 choose room page 只能看到自己有被加入其中的 room。
4. [RWD] : 有做RWD
5. [TopicKeyFunction] : 在 chat page 可 chat，load message history，並使用左上'+'鍵可新增使用者進 chat room。
6. [SignUp/SignInwithGoogle] : 如題
7. [ChromeNotification] : 登入時會跳出 notification
8. [CSSAnimation] : 在 choose room page 和 chat page 有使用 animation

# Other Functions Description(1~10%) : 
1. [ChooseRoom] : choose room 只會顯示自己所屬的 room ，並且只有在 room 中的人可以新增他人到房間中
2. [ChatpPage] : 有人輸入留言時，會自動滑到最下方

## Security Report (Optional)
1. 在留言處輸入 html 不會更動到網頁
2. 在 database.rule 中可以看到: 
   1. room message 僅該房間的 member 可以讀寫
   2. 新增成員的條件也必須是該房間的成員才能做到
   3. 他人並不能看見我屬於那些房間
   4. 他人只能新增我進房間，不能任意更改我屬於那些房間
