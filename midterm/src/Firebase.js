import * as firebase from 'firebase';

const settings = {timestampsInSnapshots: true};


  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDI-62vqtJ_p3WM56ilB4h1MfVfZ-HpO6Y",
    authDomain: "midterm-project-491bc.firebaseapp.com",
    databaseURL: "https://midterm-project-491bc.firebaseio.com",
    projectId: "midterm-project-491bc",
    storageBucket: "midterm-project-491bc.appspot.com",
    messagingSenderId: "607015919791"
  };
  firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;