import Vue from 'vue'

import '../theme/index.css'
import elementUI from 'element-ui'

import App from './App.vue'
import router from './router'
import store from './store'

Vue.use(elementUI);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
