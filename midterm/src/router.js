import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/sign-up",
      name: "sign-up",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/SignUp.vue")
    },
    {
      path: "/sign-in",
      name: "sign-in",
      component: () =>
        import(/* webpackChunkName: "sign-in" */ "./views/SignIn.vue")
    },
    {
      path: "/choose-room",
      name: "choose-room",
      component: () =>
        import(/* webpackChunkName: "choose-room" */ "./views/ChooseRoom.vue")
    },
    {
      path: "/chat-page",
      name: "chat-page",
      component: () =>
        import(/* webpackChunkName: "chat-page" */ "./views/ChatPage.vue")
    },
    {
      path: "/public-room",
      name: 'public-room',
      component: () => 
        import(/* webpackChunkName: "chat-page" */ "./views/PublicRoom.vue")
    }
  ]
});
