import Vue from 'vue'
import Vuex from 'vuex'
import firebase from './Firebase'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    roomID: 0,
    roomName: null,
    userEmail: null,
    username: null
  },
  getters: {
    getRoomID: function(state) {
      return state.roomID;
    },
    getRoomName: function(state) {
      return state.roomName;
    },
    getUserEmail: function(state) {
      return state.userEmail;
    },
    getUsername(state) {
      return state.username;
    }
  },
  mutations: {
    setRoomID: function(state, roomID) {
      state.roomID = roomID;
    },
    setRoomName: function(state, roomName) {
      state.roomName = roomName;
    },
    setUserEmail: function(state, userEmail) {
      state.userEmail = userEmail;
    },
    setUsername: function(state, username) {
      state.username = username;
    }
  },
  actions: {
    setRoomID_Name: function({ commit }, roomID) {
      commit('setRoomID', roomID);
      var roomName = "";
      if(roomID) {
        firebase.database().ref(`/roomInfo/${roomID}`).once("value", snap => {
          roomName = snap.val().roomName;
          commit('setRoomName', roomName);
        });
      }
      else {
        commit('setRoomName', null);
      }
    }
  }
})
